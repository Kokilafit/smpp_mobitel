-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2015 at 08:43 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smpp_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `smppusers`
--

CREATE TABLE IF NOT EXISTS `smppusers` (
  `EmployeeNo` int(11) NOT NULL,
  `EmailAddress` varchar(222) NOT NULL,
  `DisplayName` varchar(222) NOT NULL,
  `ContactNo` int(10) NOT NULL,
  `AccessIP` varchar(222) NOT NULL,
  `Section` varchar(222) NOT NULL,
  `ManegerName` varchar(222) NOT NULL,
  `MgrEmailAdd` varchar(222) NOT NULL,
  PRIMARY KEY (`EmployeeNo`),
  UNIQUE KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smppusers`
--

INSERT INTO `smppusers` (`EmployeeNo`, `EmailAddress`, `DisplayName`, `ContactNo`, `AccessIP`, `Section`, `ManegerName`, `MgrEmailAdd`) VALUES
(1, 'dulanjalee@gmail.com ', 'dulanjalee', 724563214, 'mobitel ', 'messaging', 'xxx', 'xxx@gmail.com'),
(2, 'deshani@gmail.com ', 'deshani', 713452376, 'mobitel ', 'roaming', 'yyy', 'yyy@gmail.com'),
(3, 'c@zdc.lk ', 'deshani', 713452376, 'csdkhfsk ', 'messaging', 'Mr.xxxx', 'bcbkckzj'),
(4, 'dulanjaleee2@gmail.com ', 'ass', 782348765, 'ew ', 'fsdhfkhsad', 'dcahkdfja', 'xxx@gmail.com'),
(5, 'dulanjaleee3@gmail.com ', 'deshani', 713452376, 'hfghk ', 'messaging', 'dcahkdfja', 'bcbkckzj'),
(6, 'c@dclk.bn ', 'dulanjalee', 713452376, 'csdkhfsk ', 'messaging', 'Mr.xxxx', 'yyy@gmail.com'),
(7, 'deshani90@gmail.com ', 'deshani', 724563214, 'csdkhfsk ', 'fsdhfkhsad', 'dcahkdfja', 'xxx@gmail.com'),
(8, 'nipuni@gmail.com ', 'nipuni', 782348765, 'mobitel ', 'bss', 'Mr.xxxx', 'xxx@gmail.com'),
(9, 'deshaniff@gmail.com ', 'ass', 786541224, 'mobitel ', 'fsdhfkhsad', 'Mr.xxxx', 'xxx@gmail.com'),
(10, 'c@dclksd.bn', 'dulanjalee', 782348765, 'csdkhfsk ', 'fsdhfkhsad', 'Mr.xxxx', 'admin@gmail.com'),
(11, 'deshani4@gmail.com ', 'dulanjalee', 713452376, 'mobitel ', 'roaming', 'Mr.xxxx', 'yyy@gmail.com'),
(12, 'deshani5@gmail.com ', 'deshani', 713452376, 'mobitel ', 'messaging', 'czbckzjx', 'xxx@gmail.com'),
(13, 'dulanjaleee6@gmail.com ', 'dulanjalee', 782348765, 'hfghk ', 'bss', 'Mr.xxxx', 'xxx@gmail.com'),
(14, 'deshani7@gmail.com ', 'ass', 724563214, 'csdkhfsk ', 'bss', 'dcahkdfja', 'yyy@gmail.com'),
(15, 'c6@dclk.bn', 'dulanjalee', 724563214, 'csdkhfsk ', 'czbczbkj', 'dcahkdfja', 'xxx@gmail.com'),
(16, 'deshani8@gmail.com ', 'dulanjalee', 724563214, 'csdkhfsk ', 'bss', 'Mr.xxxx', 'admin@gmail.com'),
(17, 'c5@dclk.bn ', 'dulanjalee', 782348765, 'mobitel ', 'messaging', 'Mr.xxxx', 'xxx@gmail.com'),
(18, 'dulanjaleee9@gmail.com ', 'dees', 724563214, 'hfghk ', 'messaging', 'Mr.xxxx', 'yyy@gmail.com'),
(19, 'c45@dclk.bn ', 'dulanjalee', 782348765, 'mobitel ', 'fsdhfkhsad', 'Mr.xxxx', 'admin@gmail.com'),
(20, 'deshani45@gmail.com ', 'dulanjalee', 724563214, 'csdkhfsk ', 'messaging', 'dcahkdfja', 'admin@gmail.com'),
(21, 'c12@dclk.bn ', 'dulanjalee', 782348765, 'csdkhfsk ', 'fsdhfkhsad', 'Mr.xxxx', 'admin@gmail.com'),
(22, 'dulanjaleee12@gmail.com ', 'dulanjalee', 786541224, 'mobitel ', 'messaging', 'Mr.xxxx', 'xxx@gmail.com'),
(23, 'sarathc12@mobitel.lk ', 'Sarath Chandrasiri', 714349574, '192.168.13.85 ', 'VAS', 'Fawaz', 'fawaz@mobitel.lk'),
(24, 'cfgh@dclk.bn ', 'asd', 56456, 'weqw ', 'asda', 'asd', 'ghjg'),
(123, 'asda56@asd.fgh', 'sdfsdf', 2147483647, '127.0.0.1', 'asdas', 'asda', 'asda@asd.fgh'),
(238, 'dulanjaleehevawithak@gmail.com', 'dula', 712346543, '127.0.0.1', 'roaming', 'sunethth perera', 'suneth@gmail.com'),
(567, 'asdqw@asd.sdf', 'asda', 2147483647, '127.0.0.1', 'VAS', 'rtyrty', 'asdqw@asd.sdf'),
(1232, 'kokila.alu@gmail.com', 'Kokila', 713626366, '127.0.0.1', 'trader', 'john snow', 'snow@gmail.com'),
(2341, 'sdfs6@sdf.dfgh', 'sdfsdf', 717894561, '127.0.0.1', 'asddd', 'asdasd', 'sdfs@sdf.dfghas'),
(2344, 'dulanjaleehevawitharana@gmail.com', 'dula', 712346543, '127.0.0.1', 'roaming', 'sunethth perera', 'suneth@gmail.com'),
(4587, 'sdfsdf@sdf.dfg', 'sdfsd', 2147483647, '127.0.0.1', 'asda', 'asdas', 'asd@asda.dfg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(222) DEFAULT NULL,
  `user_level` enum('edit','view') DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `reset_token` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `user_level`, `remember_token`, `reset_token`) VALUES
(1, 'admin', '$2y$10$WAPXjCOvTzwSs34YMEPN/ejckIiX9WUb3sAwuvLyItuN5UVYtMiQy', 'majlakshi.518@gmail.com', 'edit', 'QvDqPz8eZCdI9CoCInNTp7Db7dlagCVItCKf8D2lmgHrFVVcL9J2vGhlsyvM', ''),
(2, 'admin2', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', NULL, 'view', 'Sbz9CI3QkFlYeqjwEVprB8zIGDn6yfyktNF9PgQifdELYICOdptKyozZlIGn', NULL),
(3, 'dulanjaleehevawithak', '05ac46965ff53360dad852cb12dc9601', 'dulanjaleehevawithak@gmail.com', NULL, NULL, NULL),
(4, 'asdqw', '$2y$10$Vk7Z136RlE9ezekVzQSW7OnZPrjXLGvIofEmlSmPLBaoerRa/Mw62', 'asdqw@asd.sdf', NULL, NULL, NULL),
(5, 'sdfsdf', '$2y$10$.M2FdP3mYOpcFbaOTMUWtuqPxKxd3XTg3dwXLVuwCkSwnx4O1kz4i', 'sdfsdf@sdf.dfg', NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
