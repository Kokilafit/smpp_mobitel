<?php

class LoginController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function login()
	{
        //dd(Input::all());
        //Validating username and password------------------
        $valid = Validator::make(Input::all(),
            array(
                'username' => 'required|max:50|min:3',
                'password' => 'required|max:32|min:3'
            ));
        if(!$valid->fails()){
        //end of validation block----------------------------


        //login info check--------------------------------------------
            $auth = Auth::attempt(array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'user_level' => 'edit',
            ));
            if(!$auth) {
                $auth = Auth::attempt(array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    'user_level' => 'view'
                ));
                if($auth){
                    return Redirect::route('view_show');
                }else{
                    return Redirect::route('login')->with('notif','Login details invalid, Try again..!');
                }
            }else{
                return Redirect::route('edit_show');
            }

            }
        else{
            return Redirect::route('login')->with('notif','Login details invalid, Try again..!');
        }
	}

    public function logout(){
        Auth::logout();
        return Redirect::route('login');
    }



}
