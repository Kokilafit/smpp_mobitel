<?php

class PassResetController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function reset()
	{
        //dd(Input::all());
        //Validating username and password------------------
        $valid = Validator::make(Input::all(),
            array(
                'reset_email' => 'required|email|max:222'
            ));
        if(!$valid->fails()){
            $mail_add = DB::table('user')->select('email')->where('email',Input::get('reset_email'))->first();

            if($mail_add){
                $token = str_random(32);
                DB::table('user')
                    ->where('email',$mail_add->email )
                    ->update(array('reset_token' => $token));
                $data = array(
                    'token' => $token
                );
                Mail::send('emails.pass_reset_mail',$data, function($message) use($mail_add)
                {
                    $message->to($mail_add->email)->subject('[SMPP]Pasword Reset');
                });
                return Redirect::route('login')->with('notif','Reset Link was sent to '.$mail_add->email );
            }else{
                return Redirect::route('login')->with('notif','Email not found, Try again..!');
            }
            }
        else{
            return Redirect::route('login')->with('notif','Invalid email, Try again..!');
        }
	}

    public function checkLink($token){

        $ok = DB::table('user')->where('reset_token',$token)->first();

        if($ok){
            return View::make('passRecovery')->with(array('token'=>$token));
        }else{
            return Redirect::route('login')->with('notif','Password reset link is invalid or expired');
        }
    }

    public function setNew()
    {

        $valid = Validator::make(Input::all(),
            array(
                'reset_token' => 'required|size:32',
                'password' => 'required|max:50|min:6'
            ));
        if (!$valid->fails()) {
            $new_password = Hash::make(Input::get('password'));

            $ok = DB::table('user')
                ->where('reset_token',Input::get('reset_token'))
                ->update(array('password' => $new_password,'reset_token'=>''));
            if(!$ok){
                return Redirect::route('recovery_link',Input::get('reset_token'))->with('notif','Password reset failed ! Try again..');
            }else{
                return Redirect::route('login')->with('notif','Password Changed ! Now you can login with new password');
            }
        }

        return Redirect::route('recovery_link',Input::get('reset_token'))->with('notif','Password reset failed ! Try again');
    }
}
