<?php

class RegisterController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function register()
	{
        //dd(Input::all());
        //Validating username and password------------------
        $valid = Validator::make(Input::all(),
            array(
                'EmployeeNo' => 'required|integer|unique:smppusers',
                'display_name' => 'required|max:222|min:1',
                'ip' => 'required|ip',
                'user_email' => 'required|email',
                'contact_no' => 'required|digits:10',
                'section' => 'required|max:222|min:1',
                'manager_name' => 'required|max:222|min:1',
                'manager_email' => 'required|email',
            ));
        if(!$valid->fails()){
        //end of validation block----------------------------
            $val = DB::table('smppusers')->insert(array(
                'EmployeeNo' => Input::get('EmployeeNo'),
                'DisplayName' => Input::get('display_name'),
                'AccessIP' => Input::get('ip'),//Request::getClientIp(true),
                'EmailAddress' => Input::get('user_email'),
                'ContactNo' => Input::get('contact_no'),
                'Section' => Input::get('section'),
                'ManegerName' => Input::get('manager_name'),
                'MgrEmailAdd' => Input::get('manager_email')
            ));

            $uname = explode("@",Input::get('user_email'));

            $val2 = DB::table('user')->insert(array(
                'username' => $uname[0],
                'password' => Hash::make(Input::get('user_email')),
                'email' => Input::get('user_email')
            ));

            $data = array(
                'EmployeeNo' => Input::get('EmployeeNo'),
                'DisplayName' => Input::get('display_name'),
                'AccessIP' => Request::getClientIp(true),
                'EmailAddress' => Input::get('user_email'),
                'ContactNo' => Input::get('contact_no'),
                'Section' => Input::get('section'),
                'ManegerName' => Input::get('manager_name'),
                'MgrEmailAdd' => Input::get('manager_email')
            );

            if($val){
                Mail::send('emails.new_registration_mail',$data, function($message)
                {
                    $message->to('dulanjaleehevawitharana@gmail.com')->subject('[SMPP]new user');
                });
                return View::make('register')->with(array('notif'=>'Thank You,Your Information Saved Successful, We will reply soon !'));
            }else{
                return View::make('register')->with(array('notif'=>'Sorry Something went wrong, We are working on that !'));
            }


            }
        else{
            return View::make('register')->with(array('notif'=>'You already registered in the system'));
        }
	}


}
