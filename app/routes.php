<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'index',function(){
    return View::make('hello');
}));


Route::get('/as', array('as'=>'asd',function(){
    $data = array(
        'EmployeeNo' => Input::get('EmployeeNo'),
        'DisplayName' => Input::get('display_name'),
        'AccessIP' => Request::getClientIp(true),
        'EmailAddress' => Input::get('user_email'),
        'ContactNo' => Input::get('contact_no'),
        'Section' => Input::get('section'),
        'ManegerName' => Input::get('manager_name'),
        'MgrEmailAdd' => Input::get('manager_email')
    );


        Mail::send('emails.new_registration_mail',$data, function($message)
        {
            $message->to('majlakshi.518@gmail.com')->subject('[SMPP]new user');
        });

    dd(Mail::failures());

}));




Route::get('login', array('as'=>'login', function()
{
    return View::make('login');
}));


Route::post('login',array('as'=>'login_func','uses'=>'LoginController@login'));

Route::post('pass_recover',array('as'=>'forget_mypass','uses'=>'PassResetController@reset'));

Route::get('recovery_link/{token}',array('as'=>'recovery_link','uses'=>'PassResetController@checkLink'));


Route::post('pass_reset',array('as'=>'pass_reset','uses'=>'PassResetController@setNew'));

Route::get('logout',array('as'=>'logout_func','uses'=>'LoginController@logout'));


Route::post('login',array('as'=>'login_func','uses'=>'LoginController@login'));

Route::get('register',array('as'=>'register',function(){
    return View::make('register');
}));

Route::post('register',array('as'=>'register_func','uses'=>'RegisterController@register'));


Route::group(array('before' => 'isEditor'), function()
{
        Route::get('edit',array('as'=>'edit_show','uses'=>'EditController@index'));
});


Route::group(array('before' => 'isViewer'), function()
{
    Route::get('view',array('as'=>'view_show','uses'=>'ViewController@index'));
});