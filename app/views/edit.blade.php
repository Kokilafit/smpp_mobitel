

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Account</title>

    <link rel="stylesheet" type="text/css" href=""/>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') }}
    {{ HTML::style('public/css/edit.css') }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
    {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
</head>
<body>

<div class="container">

    <div id="head">

        <ul class="nav navbar-nav">

            <li><a href="#">Home</a></li>

            <li><a href="#">View SC</a></li>

            <li><a href="#">Edit Record</a></li>

            <li><a href="#">Add Record</a></li>


            <li><a href="#">SC Receive</a></li>

            <li><a href="#">SC/Account Request</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <div id="loginname">
                <li><span class="glyphicon glyphicon-user"></span>  <b id="welcome">Welcome : </b>
                    <b id="logout"><a href="{{Route('logout_func')}}">Log Out</a></b></li> </div>
        </ul>
    </div>

</div>
</nav>
<div class="container">
    <div class="jumbotron">
        <img src="public/images/rr.jpg">
        <div id="header3"></div>

    </div>
    <div>
        <h2>View Account</h2>
    </div>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Search</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">

                    <form class="form-horizontal" role="form" method="post" action="index.php">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Account Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Account Name" >

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Short Code:</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Short Code" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">Sender ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="account" placeholder="Sender ID" >


                            </div>
                        </div>
                        <div class="form-group">
                            <label for="human" class="col-sm-2 control-label">Product Owner:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="human" name="human" placeholder="Product Owner">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="human" class="col-sm-2 control-label">Service Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="human" name="human" placeholder="Service Name">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="human" class="col-sm-2 control-label">Application Owner:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="human" name="human" placeholder="Application Owner">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="human" class="col-sm-2 control-label">Launched Date:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="human" name="human" placeholder="Launched Date">

                            </div>
                        </div>


                        <div class="form-group">
                            <label for="human" class="col-sm-2 control-label">qqqqqqqqqq</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="human" name="human" placeholder="qqqqqqqqqq">

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">

                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>

</div>

</body>
</html>
