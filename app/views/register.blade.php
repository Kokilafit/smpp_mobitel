<html xmlns="http://www.w3.org/1999/html">
{{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') }}
{{ HTML::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/css/bootstrapvalidator.min.css') }}
{{ HTML::style('public/css/reg.css') }}
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
{{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js') }}
{{ HTML::script('public/js/validator.js') }}

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>register</title>


</head>
<body>
@if(isset($notif))
    <div class="notifications">
        {{{$notif}}}
    </div>
@endif
<div id="img">
    {{ HTML::image('public/images/ggss.jpg','Banner image',array('class'=>'banner_img img-responsive')) }}
</div>

<div class="container">
    <div class="jumbotron col-sm-10 col-sm-offset-1">
        <p class=" h4 ">User Loging Request</p>
                <form id="registering_form" class="form-horizontal" action="{{Route('register_func')}}"  method="post">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="text-center text-primary small"> User Details</p>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label small">Employee No:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="EmployeeNo" class="form-control input-sm" id="inputEmail3" placeholder="ex: 412586">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label small"> Display Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="display_name" class="form-control input-sm" id="inputPassword3" placeholder="ex: Kokila">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label small"> Access IP:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="ip"  class="form-control input-sm" id="inputPassword3" value="{{Request::getClientIp(true)}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label small">Email Address:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="user_email" class="form-control input-sm" id="inputEmail3" placeholder="ex: mymail@gmail.com">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label small">Contact No:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="contact_no" class="form-control input-sm" id="inputPassword3" placeholder="ex: 0717788778">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label small"> Section:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="section" class="form-control input-sm" id="inputPassword3" placeholder="ex: Marketing">
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="text-center text-primary small"> Manager Details</p>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label small">Manager Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="manager_name" class="form-control input-sm" id="inputEmail3" placeholder="ex: Dilshan Perera">
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label small">Manager Email:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="manager_email" class="form-control input-sm" id="inputPassword3" placeholder="ex: dilshan@yahoo.com">
                                    </div>
                                </div></div>
                        </div>
                    </div>

                    <div class="form-group" style="text-align: center">
                            <a href="{{route('login')}}" class="btn btn-default" value="Cancel">Back to Login</a>
                            <button type="submit" class="btn btn-default" value="Submit">Submit</button>
                    </div>
                </form>
    </div>
</div>

<br>

<script>
    $( document ).ready(function() {
        function explode(){
            $('.notifications').slideToggle(1500);
        }
        setTimeout(explode, 5000);
    });

</script>
</body>
</html>



