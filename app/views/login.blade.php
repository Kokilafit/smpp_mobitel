<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>login-new</title>
    {{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') }}
    {{ HTML::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/css/bootstrapvalidator.min.css') }}
    {{ HTML::style('public/css/login.css') }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
    {{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js') }}
    {{ HTML::script('public/js/login_validate.js')}}
</head>
<body>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Password Recovery</h4>
            </div>
            <form class="form-horizontal" action="{{route('forget_mypass')}}" method="post">
            <div class="modal-body">
                <h4>We will send password reset link to below email</h4>

                <div class="form-group">
                    <label for="reset_email" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="reset_email" id="reset_email" placeholder="enter your email">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send</button>

            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@if(Session::has('notif'))
    <div class="notifications">
        {{{Session::get('notif')}}}
    </div>
@endif
<div class="container">
    <div class="jumbotron">
        {{ HTML::image('public/images/ggs.jpg','Banner image',array('class'=>'banner_img img-responsive')) }}
        <br><br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="text-center">
                                    <a href="#" class="active" id="login-form-link">Login</a>
                                </div>

                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">


                                    <form id="login_form" action="{{Route('login_func')}}" method="post" >

                                        <div class="form-group">

                                            {{--<ul class="nav navbar-nav">

                                                <div id="usertype">
                                                    <label><b>User Level</b></label> <select name="usertype"><option value="admin">Edit</option><option value="other">View</option></select> </div>
                                            </ul>--}}
                                            <br>


                                            <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                        </div>
                                        {{--<div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>--}}
                                        <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3 text-center">
                                                    <Button type="submit" value="Submit" style="width: 50%" class="btn btn-login">Log In</button><br/>
                                                    <a  class="btn-link" data-toggle="modal" data-target="#myModal">Password Reset</a>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="{{route('register')}}" tabindex="5" class="forgot-password">You don't have an account yet?Register here</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <script>
            $( document ).ready(function() {
                function explode(){
                    $('.notifications').slideToggle(1500);
                }
                setTimeout(explode, 5000);
            });

        </script>





</body>
</html>

