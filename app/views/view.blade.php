<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>view</title>
    {{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') }}
    {{ HTML::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/css/bootstrapvalidator.min.css') }}
    {{ HTML::style('public/css/style-view.css') }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
    {{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
    <style>
        tr {
            height:60px;
        }
    </style>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">

        <div>
            <ul class="nav navbar-nav">

                <li><a href="#">Home</a></li>

                <li><a href="#">View SC</a></li>

                <li><a href="#">Edit Record</a></li>

                <li><a href="#">Add Record</a></li>


                <li><a href="#">SC Receive</a></li>

                <li><a href="#">SC/Account Request</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div id="loginname">
                    <li><span class="glyphicon glyphicon-user"></span>  <b id="welcome">Welcome : </i></b>
                        <b id="logout"><a href="{{Route('logout_func')}}">Log Out</a></b></li> </div>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="jumbotron">
        <h2> Short Code Request</h2>

        <div id="header3">

        </div>

    </div>
    <form>

        <TABLE BORDER="0" align="center">

            <TR>

                <TD>Enter Short Code:</TD> <td>  <INPUT TYPE="TEXT" NAME="number" SIZE="25" height="30" width="200">  </td>


            </TR>

            <TD>Purpose:</TD> <td>  <INPUT TYPE="TEXT" NAME="Purpose" SIZE="25" height="30" width="200">  </td>

            <TR>
                <TD>Request By:</TD> <td>  <INPUT TYPE="TEXT" NAME="RequestBy" SIZE="25" height="30" width="200">  </td>
            </tr>

            <tr>     <TD>Contact:</TD><td> <INPUT TYPE="TEXT" NAME="Contact" SIZE="25" height="30" width="200">    </TD>

            </TR>
            <TR>

                <TD>Expected Date of Launch</TD>
                <TD><INPUT TYPE="text" NAME="ExpectedDate" SIZE="25" height="30" width="200"></TD>
                </p>
                <h2></h2>
            </TR>
            <tr>

                <td>
                    &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp;   <input type="submit"  name="submit" value="Submit">
                </td><td>
                    <input type="submit"  name="cancel" value="Cancel">
                </td></tr>

        </TABLE>
    </form>

</div>



</body>
</html>
