<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>New User is Arrived !</h2>
        <ul>
            <li>EmployeeNo :{{$EmployeeNo}}</li>
            <li>EmailAddress :{{$EmailAddress}}</li>
            <li>DisplayName :{{$DisplayName}}</li>
            <li>ContactNo :{{$ContactNo}}</li>
            <li>AccessIP :{{$AccessIP}} </li>
            <li>Section :{{$Section}}</li>
            <li>ManegerName :{{$ManegerName}}</li>
            <li>MgrEmailAdd :{{$MgrEmailAdd}}</li>
        </ul>
		<div>
            This e-mail may contain confidential and/or privileged information. If you are not the intended recipient or have
            received this e-mail in error, please notify the sender immediately and destroy this e-mail. Any  unauthorised copying,disclosure
            or distribution of the material in this e-mail is strictly forbidden..
		</div>
	</body>
</html>
