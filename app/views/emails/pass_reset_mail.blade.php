<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password recovery mail</h2>

            if you request reset to your SMPP account password go to below link<br/><br/>
<a href="{{route('recovery_link',$token)}}">Click here</a><br/><br/>
        or
        <br/>
        {{route('recovery_link',$token)}}
        <br/>
		<div>
            This e-mail may contain confidential and/or privileged information. If you are not the intended recipient or have
            received this e-mail in error, please notify the sender immediately and destroy this e-mail. Any  unauthorised copying,disclosure
            or distribution of the material in this e-mail is strictly forbidden..
		</div>
	</body>
</html>
