<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
    {{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
    {{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}

	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}


		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="welcome">
        {{$_SERVER['REMOTE_ADDR']}}
		<a class="btn btn-default" href="{{route('login')}}" title="Laravel PHP Framework">login</a>
        <a class="btn btn-default" href="{{route('register')}}" title="Laravel PHP Framework">register</a>
        <a class="btn btn-default" href="{{route('view_show')}}" title="Laravel PHP Framework">view</a>
        <a class="btn btn-default" href="{{route('edit_show')}}" title="Laravel PHP Framework">edit</a>
		<h1>You have arrived.</h1>
	</div>
</body>
</html>
