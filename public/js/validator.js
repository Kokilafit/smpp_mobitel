/**
 * Created by kokila on 6/22/15.
 */
$(document).ready(function() {
    $('#registering_form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            EmployeeNo: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 10,
                        message: 'The Employee No must be 3 to 10 digits long'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Employee No  can only consist of digits 0-9'
                    }
                }
            },
            ip: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/,
                        message: 'Invalid IP Address'
                    }
                }
            },

            display_name: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 222,
                        message: 'The Display Name must be 3 to 200 characters long'
                    }
                }
            },
            user_email: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            contact_no: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'The Contact No should be 10 digits'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Contact No  can only consist of digits 0-9'
                    }
                }
            },
            section: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 222,
                        message: 'The section must be 3 to 200 characters long'
                    }
                }
            },
            manager_name: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 222,
                        message: 'The Manager Name must be 3 to 200 characters long'
                    }
                }
            },
            manager_email: {
                validators: {
                    notEmpty: {
                        message: 'required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
});
